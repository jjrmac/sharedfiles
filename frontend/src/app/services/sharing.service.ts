import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { download, Download } from '../components/download'
import { Observable } from 'rxjs';
import { SAVER, Saver } from '../saver.provider'


@Injectable({
  providedIn: 'root'
})
export class SharingService {

  private baseUrl = 'http://fd44-200-123-104-125.ngrok.io/FileApi';

  constructor(private http: HttpClient,
    @Inject(SAVER) private save: Saver) { }

  upload(file: File): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();

    formData.append('file', file);

    const req = new HttpRequest('POST', `${this.baseUrl}/api/file`, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.http.request(req);
  }

  getFiles(): Observable<any> {
    return this.http.get(`${this.baseUrl}/api/file`);
  }

  download(id: any, filename:string){
    console.log(filename)
    return this.http.get(`${this.baseUrl}/api/file?id=${id}`, {reportProgress: true, observe: 'events', responseType:'blob'}).pipe(download(blob => this.save(blob, filename)));
  }

  delete(id:any){
    console.log(id);
    const headers = { 'content-type': 'application/json'}  
    return this.http.post(`${this.baseUrl}/api/file/delete?id=${id}`, {}, {'headers':headers});
  }
}
