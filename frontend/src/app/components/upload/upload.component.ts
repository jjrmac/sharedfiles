import { Component, Inject, NgZone, OnInit } from '@angular/core';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SharingService } from 'src/app/services/sharing.service';
import { Download } from '../download'
import { DOCUMENT } from '@angular/common'
import Swal from 'sweetalert2';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

  selectedFiles?: FileList;
  currentFile?: File;
  progress = 0;
  message = '';
  download$: Observable<Download> | undefined

  fileInfos?: Observable<any>;

  constructor(private fileService: SharingService,
    private spinner: NgxSpinnerService,
    private ngZone:NgZone,
    @Inject(DOCUMENT) private document: Document) { }

  selectFile(event: any): void {
    this.selectedFiles = event.target.files;
  }
  actualizar(){
    this.spinner.show();
    this.ngZone.runOutsideAngular(()=> {
      this.fileInfos = this.fileService.getFiles();
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);
    });
  }
  upload(): void {
    this.progress = 0;
    if (this.selectedFiles) {
      const file: File | null = this.selectedFiles.item(0);
      if (file) {
        this.currentFile = file;
        this.fileService.upload(this.currentFile).subscribe(
          (event: any) => {
            if (event.type === HttpEventType.UploadProgress) {
              this.progress = Math.round(100 * event.loaded / event.total);
            } else if (event instanceof HttpResponse) {
              this.message = event.body.message;
              this.spinner.show();
              this.fileInfos = this.fileService.getFiles();
              setTimeout(() => {
                this.spinner.hide();
              }, 1000);
            }
          },
          (err: any) => {
            console.log(err);
            this.progress = 0;

            if (err.error && err.error.message) {
              this.message = err.error.message;
            } else {
              this.message = 'No se pudo subir el archivo';
            }

            this.currentFile = undefined;
          });
      }
      this.selectedFiles = undefined;
    }
  }

  public download(id: any, name:string):void{
    Swal.fire({
      title: 'Descarga de archivo.',
      showCancelButton: true,
      confirmButtonText: 'Descargar',
    }).then((result) => {
      if (result.isConfirmed) {
        this.fileService.download(id,name).subscribe(data => {
          this.spinner.show();
          console.log(data)
          setTimeout(() => {
            this.spinner.hide();
          }, 2000);
        });
      } 
    })
  }

  public delete(id:any){
    Swal.fire({
      title: 'Desea Eliminar el archivo?.',
      showCancelButton: true,
      confirmButtonText: 'Eliminar',
    }).then((result) => {
      if (result.isConfirmed) {
        this.fileService.delete(id).subscribe(data => {
          this.spinner.show();
          this.fileInfos = this.fileService.getFiles();
          setTimeout(() => {
            this.spinner.hide();
          }, 1000);
        });
      } 
    })
  }

  ngOnInit(): void {
    this.actualizar();
  }
}