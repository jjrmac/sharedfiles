﻿
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Net.Http;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using System.Web.Http.Description;
using System.Threading.Tasks;
using System.Net.Http.Headers;

namespace FileApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FileController : ApiController
    {
        public readonly CofeeBreakEntities context;
        public FileController() { }
        public FileController(CofeeBreakEntities context) {
            this.context = context;
        }

        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetAll() {
            try
            {
                using (var context = new CofeeBreakEntities())
                {
                    var archivos = context.Archivo.ToList();
                    string output = JsonConvert.SerializeObject(archivos);
                    var response = Request.CreateResponse(System.Net.HttpStatusCode.OK);
                    response.Content = new StringContent(output, System.Text.Encoding.UTF8, "application/json");
                    return response;
                    //return Ok(context.Archivo.ToList());
                }
            }
            catch (System.Exception ex)
            {
                return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetFile(int id)
        {
            if (id <= 0)
                return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);

            string localFilePath;

            using (var context = new CofeeBreakEntities())
            {
                var archivo = context.Archivo.Where(p => p.id == id).FirstOrDefault();
                if (archivo == null || archivo.id == 0)
                    return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, "No se encuentra el archivo");
                try
                {
                    localFilePath = archivo.path;
                    HttpResponseMessage response = new HttpResponseMessage(System.Net.HttpStatusCode.OK);
                    response.Content = new StreamContent(new FileStream(localFilePath, FileMode.Open, FileAccess.Read));
                    response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                    response.Content.Headers.ContentDisposition.FileName = string.Concat(archivo.name, ".", archivo.extension);
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

                    return response;
                }
                catch (Exception)
                {
                    Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, "No se pudo encontrar el archivo");
                }

                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound, "No se encontro el archivo");
            }
        }
           

        [System.Web.Http.HttpPost]
        public Task<HttpResponseMessage> Post()
        {
            List<Archivo> archivos = new List<Archivo>();
            List<string> savedFilePath = new List<string>();
            if (!Request.Content.IsMimeMultipartContent()) {
                throw new HttpResponseException(System.Net.HttpStatusCode.UnsupportedMediaType);
            }

            string path = System.Web.HttpContext.Current.Server.MapPath("~/Archivos");
            var provider = new MultipartFileStreamProvider(path);
                     

            var task = Request.Content.ReadAsMultipartAsync(provider).ContinueWith<HttpResponseMessage>(t =>
            {
                if (t.IsCanceled || t.IsFaulted)
                {
                    Request.CreateErrorResponse(System.Net.HttpStatusCode.InternalServerError, t.Exception);
                }
                foreach (MultipartFileData dataitem in provider.FileData)
                {
                    try
                    {
                        string fullname = dataitem.Headers.ContentDisposition.FileName.Replace("\"", "");
                        string filename = Path.GetFileNameWithoutExtension(fullname);
                        string extension = Path.GetExtension(fullname);
                        string guidName = string.Concat(Guid.NewGuid().ToString(), extension);
                        //string filePath = Path.Combine(path, Path.GetFileName(fullname));
                        string filePath = Path.Combine(path, guidName);

                        File.Move(dataitem.LocalFileName, filePath);
                        double size = Models.FileManager.GetFileSize(filePath);

                        Archivo archivo = new Archivo()
                        {
                            extension = extension.Substring(1),
                            name = filename,
                            size = size,
                            path = filePath
                        };
                        archivos.Add(archivo);

                        using (var context = new CofeeBreakEntities())
                        {
                            context.Archivo.AddRange(archivos);
                            context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        string message = ex.Message;
                    }
                }
                return Request.CreateResponse(System.Net.HttpStatusCode.Created, savedFilePath);
            });
            return task;
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage Delete(int id) {
            try
            {
                if (id <= 0)
                    Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, "No es un id valido");
                using (var context = new CofeeBreakEntities())
                {
                    var archivo = context.Archivo.Where(p => p.id == id).FirstOrDefault();
                    if (archivo == null || archivo.id == 0)
                        return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, "No se encuentra el archivo");
                    try
                    {
                        Models.FileManager.DeleteFile(archivo.path);
                    }
                    catch (Exception)
                    {
                        Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, "No se pudo eliminar el archivo");
                    }

                    context.Entry(archivo).State = System.Data.Entity.EntityState.Deleted;
                    context.SaveChanges();
                }
                return Request.CreateResponse(System.Net.HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}





